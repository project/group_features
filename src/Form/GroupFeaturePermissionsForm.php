<?php

namespace Drupal\group_features\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Access\GroupPermissionHandlerInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group\Form\GroupPermissionsTypeSpecificForm;
use Drupal\group_features\GroupFeatureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the group feature permissions administration form for a specific group type.
 */
class GroupFeaturePermissionsForm extends GroupPermissionsTypeSpecificForm {

  protected GroupFeatureInterface $groupFeature;

  /**
   * Constructs a new GroupPermissionsTypeSpecificForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\group\Access\GroupPermissionHandlerInterface $permission_handler
   *   The group permission handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, GroupPermissionHandlerInterface $permission_handler, ModuleHandlerInterface $module_handler) {
    parent::__construct($entity_type_manager, $permission_handler, $module_handler);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('group.permissions'),
      $container->get('module_handler')
    );
  }

  public function buildForm(array $form, FormStateInterface $form_state, ?GroupTypeInterface $group_type = NULL, GroupFeatureInterface $group_feature = NULL) {
    $this->groupFeature = $group_feature;

    return parent::buildForm($form, $form_state, $group_type);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Don't call `parent::submitForm`.
    return;
  }

  /**
   * {@inheritdoc}
   */
  protected function getGroupRoles() {
    $roles = parent::getGroupRoles();

    // Override the set permissions based on our group feature permissions.
    foreach ($roles as $role) {
      $perms = $this->groupFeature->getPermissions($role->id());
      $role->set('permissions', $perms);
    }

    return $roles;
  }

}
