<?php

declare(strict_types=1);

namespace Drupal\group_features\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupRoleInterface;
use Drupal\group\Entity\GroupType;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group_features\Entity\GroupFeature;

/**
 * Group Feature form.
 */
final class GroupFeatureForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $group_type = $this->getGroupType();
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [GroupFeature::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
    ];

    $form['group_type'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Group type'),
      '#maxlength' => 255,
      '#default_value' => $group_type->id(),
      '#required' => TRUE,
    ];

    $perm_form = \Drupal::formBuilder()->getForm(
      \Drupal\group_features\Form\GroupFeaturePermissionsForm::class,
      $group_type,
      $this->entity,
    );
    $form['permissions'] = $perm_form['permissions'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $group_type = $this->getGroupType();
    $roles = $this->entityTypeManager->getStorage('group_role')->loadByProperties(['group_type' => $group_type->id()]);

    $perms = [];
    foreach ($roles as $role) {
      if ($values = $form_state->getUserInput()[$role->id()] ?? FALSE) {
        $perms[$role->id()] = array_keys($values);
      }
    }
    $form_state->setValue('permissions', $perms);
    return parent::buildEntity($form, $form_state);
  }

  private function getGroupType() : ?GroupTypeInterface {
    $entity_type_manager = \Drupal::entityTypeManager();
    if (!$this->entity->isNew()) {
      GroupType::load($this->entity->getGroupType());
    }

    return \Drupal::routeMatch()->getParameter('group_type');
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    foreach ($form_state->getValue('permissions') as $role_id => $perms) {
      $group_role_storage = \Drupal::entityTypeManager()->getStorage('group_role');

      // We need to reset entity type manager cache due to us setting the permissions on the entity.
      // This will later cause a bug in the call to `revokePermissions` where the `array_diff` returns an empty array
      // thus deleting all permissions, since permissions is not the actual permissions of the group role
      // and rather what we have set in the form.
      // Resetting the cache restores the original values on the entity.
      // see: `GroupFeaturePermissionsForm::getGroupRoles`
      $group_role_storage->resetCache();

      /** @var \Drupal\group\Entity\GroupRoleInterface */
      $group_role = $group_role_storage->load($role_id);
      if ($group_role instanceof GroupRoleInterface) {
        $group_role->revokePermissions(array_values($perms))->save();
      }
    }

    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new group feature %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated group feature %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
