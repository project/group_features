<?php

declare(strict_types=1);

namespace Drupal\group_features;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\group\Entity\GroupInterface;

/**
 * Provides helping functions for group features.
 */
final class GroupFeaturesHelper implements GroupFeaturesHelperInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getFeaturesField(GroupInterface $group): ?FieldItemListInterface {
    foreach ($group->getFieldDefinitions() as $field_name => $definition) {
      if ($definition->getType() == 'group_features') {
        return $group->{$field_name};
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function isFeatureEnabled(GroupInterface $group, string $feature_name): bool {
    $features_field = $this->getFeaturesField($group);
    foreach ($features_field->getIterator() as $config) {
      /** @var \Drupal\group_features\GroupFeatureInterface $config */
      $config = $config->entity;
      if (!$config instanceof GroupFeatureInterface) {
        continue;
      }

      if ($feature_name == $config->id()) {
        return TRUE;
      }
    }

    return FALSE;
  }
}
