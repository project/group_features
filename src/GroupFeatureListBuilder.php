<?php

declare(strict_types=1);

namespace Drupal\group_features;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Provides a listing of group features.
 */
final class GroupFeatureListBuilder extends ConfigEntityListBuilder {

  protected function getEntityListQuery(): QueryInterface {
    $query = parent::getEntityListQuery();
    $query->condition('group_type', \Drupal::routeMatch()->getRawParameter('group_type'));

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\group_features\GroupFeatureInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    return $row + parent::buildRow($entity);
  }

}
