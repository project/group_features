<?php

declare(strict_types=1);

namespace Drupal\group_features\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\group\Entity\GroupRoleInterface;
use Drupal\group_features\GroupFeatureInterface;

/**
 * Defines the group feature entity type.
 *
 * @ConfigEntityType(
 *   id = "group_feature",
 *   label = @Translation("Group Feature"),
 *   label_collection = @Translation("Group Features"),
 *   label_singular = @Translation("group feature"),
 *   label_plural = @Translation("group features"),
 *   label_count = @PluralTranslation(
 *     singular = "@count group feature",
 *     plural = "@count group features",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\group_features\GroupFeatureListBuilder",
 *     "form" = {
 *       "add" = "Drupal\group_features\Form\GroupFeatureForm",
 *       "edit" = "Drupal\group_features\Form\GroupFeatureForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "group_feature",
 *   admin_permission = "administer group_feature",
 *   links = {
 *     "collection" = "/admin/group/types/manage/{group_type}/features",
 *     "add-form" = "/admin/group/types/manage/{group_type}/features/add",
 *     "edit-form" = "/admin/group/types/manage/{group_type}/features/{group_feature}",
 *     "delete-form" = "/admin/group/types/manage/{group_type}/features/{group_feature}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "group_type",
 *     "permissions",
 *   },
 * )
 */
final class GroupFeature extends ConfigEntityBase implements GroupFeatureInterface {

  /**
   * The group feature ID.
   */
  protected string $id;

  /**
   * The group feature label.
   */
  protected string $label;

  /**
   * The group feature description.
   */
  protected string $description;

  /**
   * The group type.
   */
  protected string $group_type;

  /**
   * The group permissions.
   */
  protected array $permissions = [];

  /**
   * {@inheritdoc}
   */
  public function label() {
    if ($this->isNew()) {
      return parent::label();
    }
    return sprintf("%s (%s)", $this->label, $this->get('description'));
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupType(): string {
    return $this->group_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissions(?string $role_id = NULL): array {
    if ($role_id) {
      return $this->permissions[$role_id] ?? [];
    }

    return $this->permissions;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    $uri_route_parameters['group_type'] = \Drupal::routeMatch()->getRawParameter('group_type');

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    foreach (array_keys($this->getPermissions()) as $role_id) {
      $role = \Drupal::entityTypeManager()->getStorage('group_role')->load($role_id);
      if ($role instanceof GroupRoleInterface) {
        // Add group role dependency to config.
        $dependencies->addDependency('config', $role->getConfigDependencyName());
      }
    }

    return $dependencies;
  }

}
