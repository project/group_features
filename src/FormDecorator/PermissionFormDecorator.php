<?php

namespace Drupal\group_features\FormDecorator;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\form_decorator\FormDecoratorBase;
use Psr\Container\ContainerInterface;

/**
 * @FormDecorator(
 *   hook = "form_group_admin_permissions_alter"
 * )
 */
final class PermissionFormDecorator extends FormDecoratorBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new self($configuration, $plugin_id, $plugin_definition, $container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
    protected EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function applies(): bool {
    // We only want to decorate the original group permissions form.
    return parent::applies() && \Drupal::routeMatch()->getRouteName() == 'entity.group_type.permissions_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ...$args) {
    $form = $this->inner->buildForm($form, $form_state, ...$args);

    $cell_icons_markup = $form['info']['cell_icons']['#markup'];
    $feature_cell_icons_markup = '<p>Cells with a &#9996; indicate that the permission is managed by a feature of the group_feature module.</p>';
    $form['info']['cell_icons'] = ['#markup' => $cell_icons_markup . $feature_cell_icons_markup];

    $features_storage = $this->entityTypeManager->getStorage('group_feature');
    /** @var \Drupal\group_features\Entity\GroupFeature */
    foreach ($features_storage->loadMultiple() as $feature) {
      foreach ($feature->getPermissions() as $role => $perms) {
        foreach ($perms as $perm) {
          // Replace features managed by group_feature with custom unicode character.
          if (isset($form['permissions'][$perm][$role])) {
            $form['permissions'][$perm][$role]['#type'] = 'markup;';
            $form['permissions'][$perm][$role]['#markup'] = '&#9996;';
          }
        }
      }
    }

    return $form;
  }

}
