<?php

namespace Drupal\group_features\FormDecorator;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\form_decorator\FormDecoratorBase;
use Psr\Container\ContainerInterface;

/**
 * @FormDecorator(
 *   hook = "form_field_ui_field_storage_add_form_alter"
 * )
 */
final class GroupFieldAddStorageFormDecorator extends FormDecoratorBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new self($configuration, $plugin_id, $plugin_definition, $container->get('entity_type.manager'), $container->get('current_route_match'));
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RouteMatchInterface $routeMatch,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function applies(): bool {
    return parent::applies() && $this->routeMatch->getRouteName() == 'field_ui.field_storage_config_add_group';
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    /** @var \Drupal\group\Entity\GroupTypeInterface */
    $group_type = $this->routeMatch->getRawParameter('group_type');
    $storage_type = $form_state->getValue('new_storage_type');
    if ($storage_type == 'group_features') {
      $fields = $this->entityTypeManager->getStorage('field_config')->loadByProperties([
        'entity_type' => 'group',
        'bundle' => $group_type,
        'field_type' => $storage_type,
      ]);
      if (count($fields) > 0) {
        $form_state->setErrorByName('new_storage_type', $this->t('There can only exist one field of type "Features" on this group type.'));
      }
    }
  }

}
