<?php

namespace Drupal\group_features\Plugin\views\access;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Access\GroupPermissionHandlerInterface;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Access plugin that provides group feature-based access control.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "group_feature",
 *   title = @Translation("Group feature"),
 *   help = @Translation("Access will be granted when the group has the feature enabled and the containing permissions match.")
 * )
 */
class GroupFeatureViewsAccessCheck extends AccessPluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = TRUE;

  /**
   * The group permission handler.
   *
   * @var \Drupal\group\Access\GroupPermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The group entity from the route.
   *
   * @var \Drupal\group\Entity\GroupInterface
   */
  protected $group;

  /**
   * The group context from the route.
   *
   * @var \Drupal\Core\Plugin\Context\ContextInterface
   */
  protected $context;

  /**
   * Constructs a Permission object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\group\Access\GroupPermissionHandlerInterface $permission_handler
   *   The group permission handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Plugin\Context\ContextProviderInterface $context_provider
   *   The group route context.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GroupPermissionHandlerInterface $permission_handler, ModuleHandlerInterface $module_handler, protected readonly ContextRepositoryInterface $contextRepository, protected readonly EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->permissionHandler = $permission_handler;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('group.permissions'),
      $container->get('module_handler'),
      $container->get('context.repository'),
      $container->get('entity_type.manager'),
    );
  }

  public function init(ViewExecutable $view, DisplayPluginBase $display, ?array &$options = NULL) {
    parent::init($view, $display, $options);

    $this->context = $this->contextRepository->getAvailableContexts()[$this->options['context_mapping']];
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    $this->group = $this->context->getContextValue();
    if (empty($this->group)) {
      return FALSE;
    }

    // Collect feature permissions.
    $permissions = [];
    /** @var \Drupal\group_features\Entity\GroupFeature */
    $config = $this->entityTypeManager->getStorage('group_feature')->load($this->options['group_feature']);

    /** @var \Drupal\group\Entity\Storage\GroupRoleStorageInterface */
    $group_role_storage = $this->entityTypeManager->getStorage('group_role');
    $roles = $group_role_storage->loadByUserAndGroup($account, $this->group);

    $permissions = [];
    foreach ($roles as $role) {
      $role_permissions = $config->getPermissions($role->id());
    }

    $permissions = array_unique(array_merge($permissions, $role_permissions));
    // Re-use group permission logic.
    // Allow to conjunct the permissions with OR ('+') or AND (',').
    if (count($permissions) > 1) {
      return GroupAccessResult::allowedIfHasGroupPermissions($this->group, $account, $permissions, 'AND')->isAllowed();
    }
    else {
      return GroupAccessResult::allowedIfHasGroupPermissions($this->group, $account, $permissions, 'OR')->isAllowed();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_group_feature', $this->options['group_feature']);
    $route->setOption('parameters', ['group' => ['type' => 'entity:group']]);
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    $config = $this->entityTypeManager->getStorage('group_feature')->load($this->options['group_feature']);
    if (!$config) {
      return $this->t($this->options['group_feature']);
    }

    return $this->t($config->label());
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['group_feature'] = ['default' => ''];
    $options['context_mapping'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Get list of features.
    $features = $this->entityTypeManager->getStorage('group_feature')->loadMultiple();
    /** @var \Drupal\group_features\GroupFeatureInterface */
    foreach ($features as $feature) {
      $features[$feature->id()] = $feature->label();
    }

    $form['group_feature'] = [
      '#type' => 'select',
      '#options' => $features,
      '#title' => $this->t('Group features'),
      '#default_value' => $this->options['group_feature'],
      '#description' => $this->t('Only groups with the selected group features will be able to access this display.'),
    ];

    $context_options = [];
    $contexts = $this->contextRepository->getAvailableContexts();
    foreach ($contexts as $key => $context) {
      if ($context->getContextDefinition()->getDataType() == 'entity:group') {
        $context_options[$key] = $context->getContextDefinition()->getLabel();
      }
    }

    $form['context_mapping'] = [
      '#type' => count($context_options) == 1 ? 'hidden' : 'select',
      '#options' => $context_options,
      '#title' => $this->t('Context'),
      '#default_value' => $this->options['context_mapping'],
      '#description' => $this->t('Context definition to load the route from'),
      '#required' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::mergeMaxAges(Cache::PERMANENT, $this->context->getCacheMaxAge());
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(['user.is_group_member', 'user.group_permissions'], $this->context->getCacheContexts());
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return $this->context->getCacheTags();
  }

}
