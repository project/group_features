<?php

declare(strict_types=1);

namespace Drupal\group_features\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Defines the 'group_features' field type.
 *
 * @FieldType(
 *   id = "group_features",
 *   label = @Translation("Features"),
 *   description = @Translation("Select Group Features you want to use."),
 *   default_widget = "options_buttons",
 *   default_formatter = "entity_reference_label",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 * )
 */
final class GroupFeaturesItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $settings = parent::defaultStorageSettings();

    // We only want to select `group_feature` entities.
    $settings['target_type'] = 'group_feature';

    return $settings;
  }

}
