<?php

namespace Drupal\group_features\Access;

use Drupal\group\Entity\GroupInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Access\GroupAccessResult;
use Symfony\Component\Routing\Route;

/**
 * Determines access to routes based on features defined via
 * group feature.
 */
class GroupFeatureAccessCheck implements AccessInterface {

  /**
   * Checks access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check access for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $feature = $route->getRequirement('_group_feature');

    // Don't interfere if no feature was specified.
    if ($feature === NULL) {
      return AccessResult::neutral();
    }

    // Don't interfere if no group was specified.
    $parameters = $route_match->getParameters();
    if (!$parameters->has('group')) {
      return AccessResult::neutral();
    }

    // Don't interfere if the group isn't a real group.
    $group = $parameters->get('group');
    if (!$group instanceof GroupInterface) {
      return AccessResult::neutral();
    }

    /** @var \Drupal\group_features\GroupFeaturesHelperInterface */
    $helper = \Drupal::service('group_features.helper');
    $is_enabled = $helper->isFeatureEnabled($group, ltrim($feature, "!"));
    if (str_starts_with($feature, "!")) {
      $is_enabled = !$is_enabled;
    }
    return AccessResult::allowedIf($is_enabled);
  }

}
