<?php

namespace Drupal\group_features\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\flexible_permissions\CalculatedPermissionsItem;
use Drupal\flexible_permissions\PermissionCalculatorBase;
use Drupal\group\Context\GroupRouteContextTrait;
use Drupal\group_features\GroupFeatureInterface;
use Drupal\group_features\GroupFeaturesHelperInterface;

/**
 * Calculate feature group permissions.
 */
class FeatureGroupPermissionCalculator extends PermissionCalculatorBase {

  use GroupRouteContextTrait;

  /**
   * Constructs a FeatureGroupPermissionCalculator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    private readonly GroupFeaturesHelperInterface $groupFeaturesHelper,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function calculatePermissions(AccountInterface $account, $scope) {
    /** @var \Drupal\flexible_permissions\CalculatedPermissionsInterface */
    $calculated_permissions = parent::calculatePermissions($account, $scope);

    // The cache will be invalidated and the permissions recalculated
    // when the groups feature selection might be updated
    // or a group feature itself has a change (changed permissions).

    // TODO: Use bundle (only those who have the feature field) for cache tag.
    $calculated_permissions->addCacheTags(['group_list']);
    $calculated_permissions->addCacheTags(['config:group_feature']);

    // We need to load all groups, previously we got the group from route
    // which is not compatible with the "calculate once for all
    // and invalidate when needed" concept. As just viewing another group
    // would not have an effect on permissions at all.
    $all_groups = \Drupal::entityTypeManager()->getStorage('group')->loadMultiple();
    foreach ($all_groups as $group) {
      $features_field = $this->groupFeaturesHelper->getFeaturesField($group);
      if (!$features_field) {
        // This group type does not appear to have a features field.
        // Evaluate the next group.
        // TODO: Maybe we could only load groups of types that have the field
        // in the first place?
        continue;
      }

      $collected_permissions = [];
      foreach ($features_field->getIterator() as $config) {
        /** @var \Drupal\group_features\GroupFeatureInterface $config */
        $config = $config->entity;
        if (!$config instanceof GroupFeatureInterface) {
          // The Feature might have been deleted, but is still selected.
          continue;
        }
        $our_group_roles = \Drupal::entityTypeManager()->getStorage('group_role')->loadByUserAndGroup($account, $group);
        foreach ($config->getPermissions() as $group_role => $perms) {
          // We have the group role, grant the the permission.
          if (in_array($group_role, array_keys($our_group_roles))) {
            $collected_permissions = array_unique(array_merge($collected_permissions, $perms));
          }
        }
      }

      // Build item with granted permissions.
      if (!empty($calculated_permissions)) {
        $calculated_permissions->addItem(new CalculatedPermissionsItem(
          $scope,
          $group->id(),
          $collected_permissions,
          FALSE,
        ));
      }
    }
    return $calculated_permissions;
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistentCacheContexts($scope) {
    return [];
  }

}
