<?php

declare(strict_types=1);

namespace Drupal\group_features;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\group\Entity\GroupInterface;

/**
 * Constructs a GroupFeaturesHelper object.
 */
interface GroupFeaturesHelperInterface {

  /**
   * Gets a features field if available on the group.
   */
  public function getFeaturesField(GroupInterface $group): ?FieldItemListInterface;

  /**
   * Checks if a feature is anbled.
   */
  public function isFeatureEnabled(GroupInterface $group, string $feature_name): bool;

}
