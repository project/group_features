<?php

declare(strict_types=1);

namespace Drupal\group_features;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a group feature entity type.
 */
interface GroupFeatureInterface extends ConfigEntityInterface {

  /**
   * Gets the group type.
   */
  public function getGroupType(): string;

  /**
   * Gets the permissions.
   */
  public function getPermissions(?string $role_id = NULL): array;

}
