# Group Features
Module to implement opt-in group features for groups.
A group feature contains of a set of permissions which are given depending on the feature being selected on the group.

This provides flexible use of functionality that is shared among the same group type but can be enabled independently.

## How to use
1. Add a Feature field to a group.
2. Create a Feature and select its permissions.
3. Enable the feature on the group.

## Access Check mechanisms

### Route Requirement
`_group_feature: featurename`

### In Views
Select "Group Feature" Views Access Plugin.

The Context how to retrieve the group can be selected.
